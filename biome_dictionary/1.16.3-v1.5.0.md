All of these biome dictionary entries start with "structure_gel:" unless otherwise specified. Other mods are able to register their own entries, so it's up to them to document them. These are only the ones natively found in Structure Gel.

As of v1.5.0, this biome dictionary has built-in support for vanilla biomes, Biomes O' Plenty, Oh The Biomes You'll Go, Endergetic Expansion, Nethercraft, Rediscovered, Moolands, Pagamos, Glacidus, and Good Night's Sleep. As a result, some tags only apply if those mods are installed, such as structure_gel:nether_fleshy.

When loading the game, any biome not registered to the structure gel biome dictionary will automatically be registered using vanilla and forge values. This should account for any mod not natively supported as long as they register biomes through forge registries. Since it may not be perfect, especially with dimensions, you can disable this in the config completely or on a mod-specific basis.

# Similar Appearance
For the most part, these are what they sound like. structure_gel:ocean is every ocean biome, while structure_gel:frozen_ocean is specifically for frozen ones.
- ocean
- frozen_ocean
- cold_ocean
- warm_ocean
- plains
- snowy_plains : Flat areas with a lot of snow.
- desert
- savanna
- mountain_savanna
- flowery : Any biome with a notable amount of flowers.
- wooded : Any biome filled with small to medium sized trees, like oak and birch forests.
- large_wooded : Any biome filled with large trees, like jungles and redwood forests.
- oak_forest
- birch_forest
- spruce_forest
- snowy_spruce_forest
- large_spruce_forest
- jungle
- bamboo_jungle
- dark_forest
- coniferous_forest
- redwood_forest
- autumn_forest
- cherry_forest
- bamboo
- mountain
- snowy_mountain
- swamp
- mangrove
- badlands : Mesas and similar "feeling" biomes. Think western cowboy movie.
- mushroom : Biomes with large mushrooms.
- fungal : Biomes with any amount of mushrooms, mycelium, or similar.
- river
- lake
- beach
- sandy
- gravelly
- dirty : Biomes with a lot of dirt or mud on the ground.
- tropical : Different from jungle. This is more along the lines of palm trees and coconuts.
- volcanic
- dead : Biomes that are all dried out and tend to have dead plants all around.
- sky : Floating island biomes.

# Similar Temperature
These are biomes with similar temperature themes, going from absolute cold to literally on fire.
- frozen
- snowy
- cold
- neutral_temp : Includes biomes like plains and forests.
- warm
- hot
- fiery

# Similar Humidity
This kind of explains itself. Jungles are humid and deserts are dry.
- humid : Jungles, swamps, etc.
- dry : Deserts, badlands, savannas, etc.

# Special
These biomes don't really fit the other groups, so I put them here.
- void : Yep. It's the void biome. A bit of an obscure tag, but it could have a niche use.
- magical
- spooky
- rare
- space
- pumpkin

# Nether
All biomes found within The Nether.
- overgrown_nether : Areas with a lot of plant life.
- warped
- crimson
- nether_forest : Areas filled with trees. Crimson and warped forests are included.
- nether_fungal : Areas filled with fungal growth. Also includes crimson and warped forests.
- nether_sandy : Sand all over the place, like the soul sand valley.
- nether_extreme : Areas with rough terrain, like the basalt delta.
- nether_fleshy : Flesh. Flesh everywhere. It's so gross but I had to make it an entry.

# End
All biomes found within The End.
- outer_end_island : These are the islands outside of the center island. This is what you'll mainly use for world generation since there's solid land here.
- outer_end : All biomes outside of the main dragon fight island.

# Dimension
All biomes associated with the provided dimension.

## Vanilla
- nether : Supports vanilla, Biomes O' Plenty, Oh The Biomes You'll Go, and Nethercraft.
- end : Supports vanilla, Oh The Biomes You'll Go, and Endergetic Expansion.
- overworld : Supports vanilla, Biomes O' Plenty, and Oh The Biomes You'll Go.

## Modded
- aether : The Aether
- skylands : Rediscovered
- moolands : Moolands
- pagamos : Pagamos
- glacidus : Glacidus
- good_dream : Good Night's Sleep
- nightmare : Good Night's Sleep

# Forge
The following entries are pointers to the Forge biome dictionary.
- forge:hot
- forge:cold
- forge:sparse
- forge:dense
- forge:wet
- forge:dry
- forge:savanna
- forge:coniferous
- forge:jungle
- forge:spooky
- forge:dead
- forge:lush
- forge:mushroom
- forge:magical
- forge:rare
- forge:plateau
- forge:modified
- forge:ocean
- forge:river
- forge:water
- forge:mesa
- forge:forest
- forge:plains
- forge:mountain
- forge:hills
- forge:swamp
- forge:sandy
- forge:snowy
- forge:wasteland
- forge:beach
- forge:void
- forge:overworld
- forge:nether
- forge:end