package com.legacy.structure_gel.registrars;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.legacy.structure_gel.worldgen.structure.GelStructure;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.structure.IStructurePieceType;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * A variation of {@link StructureRegistrar} designed to handle
 * {@link GelStructure}s specifically. Automatically populates separation
 * settings.
 *
 * @param <C>
 * @param <S>
 * @author David
 */
public class GelStructureRegistrar<C extends IFeatureConfig, S extends GelStructure<C>> extends StructureRegistrar2<C, S>
{
	/**
	 * The most simple structure with only one config and one piece type
	 *
	 * @param name
	 * @param structure
	 * @param pieceType
	 * @param config
	 * @param generationStage
	 */
	public GelStructureRegistrar(ResourceLocation name, S structure, IStructurePieceType pieceType, C config, GenerationStage.Decoration generationStage)
	{
		this(name, structure, ImmutableMap.of("", pieceType), ImmutableMap.of("", config), generationStage);
	}

	/**
	 * A structure with one piece type and multiple configured features.
	 * Generally those would be used for different biomes, like how villages
	 * work
	 *
	 * @param name
	 * @param structure
	 * @param pieceType
	 * @param configs
	 * @param generationStage
	 */
	public GelStructureRegistrar(ResourceLocation name, S structure, IStructurePieceType pieceType, Map<String, C> configs, GenerationStage.Decoration generationStage)
	{
		this(name, structure, ImmutableMap.of("", pieceType), configs, generationStage);
	}

	/**
	 * A structure with multiple piece types and one config. Generally this
	 * would be used for a structure with a lot of complexity in its pieces
	 *
	 * @param name
	 * @param structure
	 * @param pieceTypes
	 * @param config
	 * @param generationStage
	 */
	public GelStructureRegistrar(ResourceLocation name, S structure, Map<String, IStructurePieceType> pieceTypes, C config, GenerationStage.Decoration generationStage)
	{
		this(name, structure, pieceTypes, ImmutableMap.of("", config), generationStage);
	}

	/**
	 * A structure with multiple piece types and multiple configs. This would
	 * be for something with many pieces and different configured variations.
	 * Probably the most rare type to need
	 *
	 * @param name
	 * @param structure
	 * @param pieceTypes
	 * @param configs
	 * @param generationStage
	 */
	public GelStructureRegistrar(ResourceLocation name, S structure, Map<String, IStructurePieceType> pieceTypes, Map<String, C> configs, GenerationStage.Decoration generationStage)
	{
		super(name, structure, pieceTypes, configs, generationStage, structure::getSeparationSettings);
	}

	/**
	 * Handy method so you don't have to type the generic type parameters.
	 *
	 * @param name
	 * @param structure
	 * @param pieceType
	 * @param configs
	 * @param generationStage
	 * @return {@link GelStructureRegistrar}
	 */
	public static <C extends IFeatureConfig, S extends GelStructure<C>> GelStructureRegistrar<C, S> of(ResourceLocation name, S structure, IStructurePieceType pieceType, Map<String, C> configs, GenerationStage.Decoration generationStage)
	{
		return new GelStructureRegistrar<C, S>(name, structure, pieceType, configs, generationStage);
	}

	/**
	 * Handy method so you don't have to type the generic type parameters.
	 *
	 * @param name
	 * @param structure
	 * @param pieceType
	 * @param config
	 * @param generationStage
	 * @return {@link GelStructureRegistrar}
	 */
	public static <C extends IFeatureConfig, S extends GelStructure<C>> GelStructureRegistrar<C, S> of(ResourceLocation name, S structure, IStructurePieceType pieceType, C config, GenerationStage.Decoration generationStage)
	{
		return new GelStructureRegistrar<C, S>(name, structure, pieceType, config, generationStage);
	}

	/**
	 * Handy method so you don't have to type the generic type parameters.
	 *
	 * @param name
	 * @param structure
	 * @param pieceTypes
	 * @param configs
	 * @param generationStage
	 * @return {@link GelStructureRegistrar}
	 */
	public static <C extends IFeatureConfig, S extends GelStructure<C>> GelStructureRegistrar<C, S> of(ResourceLocation name, S structure, Map<String, IStructurePieceType> pieceTypes, Map<String, C> configs, GenerationStage.Decoration generationStage)
	{
		return new GelStructureRegistrar<C, S>(name, structure, pieceTypes, configs, generationStage);
	}

	/**
	 * Handy method so you don't have to type the generic type parameters.
	 *
	 * @param name
	 * @param structure
	 * @param pieceTypes
	 * @param config
	 * @param generationStage
	 * @return {@link GelStructureRegistrar}
	 */
	public static <C extends IFeatureConfig, S extends GelStructure<C>> GelStructureRegistrar<C, S> of(ResourceLocation name, S structure, Map<String, IStructurePieceType> pieceTypes, C config, GenerationStage.Decoration generationStage)
	{
		return new GelStructureRegistrar<C, S>(name, structure, pieceTypes, config, generationStage);
	}
	
	@Override
	public GelStructureRegistrar<C, S> handle()
	{
		return (GelStructureRegistrar<C, S>) super.handle();
	}
	
	@Override
	public GelStructureRegistrar<C, S> handleForge(IForgeRegistry<Structure<?>> registry)
	{
		return (GelStructureRegistrar<C, S>) super.handleForge(registry);
	}
}
