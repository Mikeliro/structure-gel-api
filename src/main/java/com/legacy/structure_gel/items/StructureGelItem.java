package com.legacy.structure_gel.items;

import com.legacy.structure_gel.StructureGelMod;
import com.legacy.structure_gel.blocks.IStructureGel.Behavior;
import com.legacy.structure_gel.blocks.IStructureGel.IBehavior;
import com.legacy.structure_gel.blocks.StructureGelBlock;
import net.minecraft.item.*;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.List;

/**
 * The item used by structure gel blocks to display information on how it works.
 *
 * @author David
 */
public class StructureGelItem extends BlockItem
{
	/**
	 * @param blockIn : must be an instanceof {@link StructureGelBlock}
	 * @see StructureGelItem
	 */
	public StructureGelItem(StructureGelBlock blockIn)
	{
		super(blockIn, new Item.Properties().tab(ItemGroup.TAB_MISC).stacksTo(blockIn.behaviors.contains(Behavior.DYNAMIC_SPREAD_DIST) ? 50 : 64));
	}

	/**
	 * Prevents the player from placing if they are not in creative mode.
	 */
	@Override
	public ActionResultType place(BlockItemUseContext context)
	{
		if (!context.getPlayer().isCreative())
			return ActionResultType.FAIL;
		else
			return super.place(context);
	}

	/**
	 * Displays information about the structure gel based on it's behaviors.
	 *
	 * @see Behavior
	 */
	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, net.minecraft.client.util.ITooltipFlag flagIn)
	{
		if (!net.minecraft.client.gui.screen.Screen.hasShiftDown())
			tooltip.add(new TranslationTextComponent("info." + StructureGelMod.MODID + ".hold_shift").withStyle(TextFormatting.GRAY));
		else
		{
			if (this.getBlock() instanceof StructureGelBlock)
			{
				tooltip.add(new TranslationTextComponent("info." + StructureGelMod.MODID + ".place").withStyle(TextFormatting.GRAY));
				tooltip.add(new TranslationTextComponent("info." + StructureGelMod.MODID + ".gunpowder").withStyle(TextFormatting.GRAY));
				tooltip.add(new TranslationTextComponent(""));

				if (((StructureGelBlock) this.getBlock()).behaviors.isEmpty())
					tooltip.add(new TranslationTextComponent(Behavior.DEFAULT.getTranslation()).withStyle(TextFormatting.GRAY));

				for (IBehavior behavior : ((StructureGelBlock) this.getBlock()).behaviors)
					tooltip.add(new TranslationTextComponent(behavior.getTranslation()).withStyle(TextFormatting.GRAY));
			}
			else
				tooltip.add(new TranslationTextComponent("info." + StructureGelMod.MODID + ".unknown_behavior").withStyle(TextFormatting.RED));
		}
	}
}
