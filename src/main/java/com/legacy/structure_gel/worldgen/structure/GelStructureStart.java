package com.legacy.structure_gel.worldgen.structure;

import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureStart;

/**
 * An extension of {@link StructureStart} with a few methods for adjusting the y
 * coordinate of your structure. Not sure how useful it is, but it doesn't hurt
 * to keep.
 *
 * @param <C>
 * @author David
 */
public abstract class GelStructureStart<C extends IFeatureConfig> extends StructureStart<C>
{
	public GelStructureStart(Structure<C> structureIn, int chunkX, int chunkZ, MutableBoundingBox boundsIn, int referenceIn, long seed)
	{
		super(structureIn, chunkX, chunkZ, boundsIn, referenceIn, seed);
	}

	/**
	 * Places the structure at a y value between the two values entered such that
	 * the top of it's bounding box is below maxY and the bottom of it's bounding
	 * box is above minY.<br>
	 * <br>
	 * If minY is bigger than maxY, or vis versa, it automatically corrects it.
	 *
	 * @param minY
	 * @param maxY
	 */
	public void setHeight(int minY, int maxY)
	{
		if (maxY < minY)
			maxY = minY + 1;
		if (minY > maxY)
			minY = maxY - 1;

		if (maxY < this.boundingBox.y1)
		{
			int offsetAmount = this.boundingBox.y1 - maxY;
			this.boundingBox.move(0, -offsetAmount, 0);
			this.pieces.forEach(piece -> piece.move(0, -offsetAmount, 0));
		}

		if (minY > this.boundingBox.y0)
		{
			int offsetAmount = minY - this.boundingBox.y0;
			this.boundingBox.move(0, offsetAmount, 0);
			this.pieces.forEach(piece -> piece.move(0, offsetAmount, 0));
		}
	}

	/**
	 * Places the structure at the y value set.
	 *
	 * @param y
	 */
	public void setHeight(int y)
	{
		int offset = y - this.boundingBox.y0;
		this.boundingBox.move(0, offset, 0);
		this.pieces.forEach(piece -> piece.move(0, offset, 0));
	}
}
