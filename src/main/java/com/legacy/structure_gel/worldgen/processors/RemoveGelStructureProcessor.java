package com.legacy.structure_gel.worldgen.processors;

import com.legacy.structure_gel.SGRegistry.Processors;
import com.legacy.structure_gel.data.GelTags;
import com.legacy.structure_gel.util.Internal;
import com.legacy.structure_gel.worldgen.jigsaw.GelJigsawPiece;
import com.legacy.structure_gel.worldgen.jigsaw.GelStructurePiece;
import com.mojang.serialization.Codec;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.gen.feature.template.*;

import javax.annotation.Nullable;

/**
 * Functions similar to
 * {@link BlockIgnoreStructureProcessor#STRUCTURE_AND_AIR} but also
 * replaces blocks tagged as {@link GelTags#GEL} with air. Structure blocks are
 * not ignored as they can be used in jigsaw structures with
 * {@link GelStructurePiece} Great to make sure the insides of structures don't
 * have unintended blocks inside of them, and to make it easier to blend them
 * into the world.<br>
 * <br>
 * This processor is automatically added to anything using
 * {@link GelJigsawPiece}.
 *
 * @author David
 */
public class RemoveGelStructureProcessor extends StructureProcessor
{
	public static final RemoveGelStructureProcessor INSTANCE = new RemoveGelStructureProcessor();
	public static final Codec<RemoveGelStructureProcessor> CODEC = Codec.unit(() -> INSTANCE);

	/**
	 *
	 */
	@Nullable
	@Internal
	@Override
	public Template.BlockInfo processBlock(IWorldReader worldReaderIn, BlockPos pos, BlockPos pos2, Template.BlockInfo existing, Template.BlockInfo placed, PlacementSettings settings)
	{
		if (placed.state.getBlock().is(GelTags.GEL))
			return new Template.BlockInfo(placed.pos, Blocks.AIR.defaultBlockState(), null);
		return placed.state.getBlock() == Blocks.AIR ? null : placed;
	}

	/**
	 *
	 */
	@Internal
	@Override
	protected IStructureProcessorType<?> getType()
	{
		return Processors.REMOVE_FILLER;
	}
}
