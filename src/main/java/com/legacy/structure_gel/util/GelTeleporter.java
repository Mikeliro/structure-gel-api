package com.legacy.structure_gel.util;

import com.legacy.structure_gel.blocks.GelPortalBlock;
import net.minecraft.block.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.TeleportationRepositioner;
import net.minecraft.util.TeleportationRepositioner.Result;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.village.PointOfInterest;
import net.minecraft.village.PointOfInterestManager;
import net.minecraft.village.PointOfInterestType;
import net.minecraft.world.DimensionType;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.border.WorldBorder;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A more mod compatible teleporter for the API.
 *
 * @author David
 */
public class GelTeleporter extends Teleporter
{
	/**
	 * The default dimension this teleporter will take you to.
	 */
	private final Supplier<RegistryKey<World>> dimension1;
	/**
	 * The dimension this teleporter will take you to.
	 */
	private final Supplier<RegistryKey<World>> dimension2;
	/**
	 * The PointOfInterestType representing the portal.
	 */
	private final Supplier<PointOfInterestType> portalPOI;
	/**
	 * The portal block that gets placed. Must extend {@link GelPortalBlock}.
	 */
	private final Supplier<GelPortalBlock> portalBlock;
	/**
	 * The {@link BlockState} of the frame that will get placed.
	 */
	private final Supplier<BlockState> frameBlock;
	/**
	 * The logic behind how creating a new portal will work.
	 *
	 * @see ICreatePortalFuncion
	 * @see CreatePortalBehavior
	 */
	private final ICreatePortalFuncion placementBehavior;

	public GelTeleporter(ServerWorld world, Supplier<RegistryKey<World>> dimension1, Supplier<RegistryKey<World>> dimension2, Supplier<PointOfInterestType> portalPOI, Supplier<GelPortalBlock> portalBlock, Supplier<BlockState> frameBlock, ICreatePortalFuncion placementBehavior)
	{
		super(world);
		this.dimension1 = dimension1;
		this.dimension2 = dimension2;
		this.portalPOI = portalPOI;
		this.portalBlock = portalBlock;
		this.frameBlock = frameBlock;
		this.placementBehavior = placementBehavior;
	}

	public GelTeleporter(ServerWorld worldIn, Supplier<RegistryKey<World>> dimension1, Supplier<RegistryKey<World>> dimension2, Supplier<PointOfInterestType> portalPOI, Supplier<GelPortalBlock> portalBlock, Supplier<BlockState> frameBlock, CreatePortalBehavior placementBehavior)
	{
		this(worldIn, dimension1, dimension2, portalPOI, portalBlock, frameBlock, placementBehavior.get());
	}

	/**
	 * Returns the opposite world from the one in this instance, or
	 * {@link #dimension1} by default.
	 *
	 * @return {@link RegistryKey}
	 */
	@Internal
	public RegistryKey<World> getOpposite()
	{
		if (this.level != null && this.level.dimension().location().equals(this.dimension1.get().location()))
			return this.dimension2.get();
		else
			return this.dimension1.get();
	}

	public Supplier<RegistryKey<World>> getDimension1()
	{
		return this.dimension1;
	}

	public Supplier<RegistryKey<World>> getDimension2()
	{
		return this.dimension2;
	}

	public Supplier<PointOfInterestType> getPortalPOI()
	{
		return this.portalPOI;
	}

	public Supplier<GelPortalBlock> getPortalBlock()
	{
		return this.portalBlock;
	}

	public Supplier<BlockState> getFrameBlock()
	{
		return this.frameBlock;
	}

	public ICreatePortalFuncion getPlacementBehavior()
	{
		return this.placementBehavior;
	}

	/**
	 * Gets the destination world of this teleporter.
	 *
	 * @return {@link ServerWorld}
	 */
	public ServerWorld getWorld()
	{
		return this.level;
	}

	/**
	 * Returns if the block passed should be ignored when placing on the surface. By
	 * default, ignores blocks tagged as leaves or logs, air, and blocks without
	 * collision (except fluids).
	 *
	 * @param state
	 * @param pos
	 * @return {@link Boolean}
	 */
	public boolean shouldIgnoreBlock(BlockState state, BlockPos pos)
	{
		return state.is(BlockTags.LEAVES) || state.is(BlockTags.LOGS) || level.isEmptyBlock(pos) || (state.getCollisionShape(level, pos).isEmpty() && !state.getMaterial().isLiquid());
	}

	/**
	 * Returns the default height to generate a portal if a position cannot be found
	 * when placing on surface. Defaults to 70.
	 *
	 * @return {@link Integer}
	 */
	public int getDefaultHeight()
	{
		return 70;
	}

	/**
	 * Locates a portal in the {@link #getWorld()}.
	 */
	@Override
	public Optional<TeleportationRepositioner.Result> findPortalAround(BlockPos startPos, boolean toNether)
	{
		PointOfInterestManager poiManager = this.level.getPoiManager();
		int dist = (int) Math.max(DimensionType.getTeleportationScale(this.level.getServer().getLevel(this.getOpposite()).dimensionType(), this.level.dimensionType()) * 16, 16);
		poiManager.ensureLoadedAndValid(this.level, startPos, dist);

		Optional<PointOfInterest> optional = poiManager.getInSquare(poiType -> poiType == this.portalPOI.get(), startPos, dist, PointOfInterestManager.Status.ANY).filter(poi -> poiManager.getType(poi.getPos().below()).orElse(null) != poi.getPoiType()).filter(poi -> this.level.getBlockState(poi.getPos()).hasProperty(BlockStateProperties.HORIZONTAL_AXIS)).min(Comparator.<PointOfInterest>comparingDouble(poi -> poi.getPos().distSqr(startPos)).thenComparingInt(poi -> poi.getPos().getY()));

		return optional.map(poi ->
		{
			BlockPos blockpos = poi.getPos();
			this.level.getChunkSource().addRegionTicket(TicketType.PORTAL, new ChunkPos(blockpos), 3, blockpos);
			BlockState blockstate = this.level.getBlockState(blockpos);
			return TeleportationRepositioner.getLargestRectangleAround(blockpos, blockstate.getValue(BlockStateProperties.HORIZONTAL_AXIS), 21, Direction.Axis.Y, 21, (pos) -> this.level.getBlockState(pos) == blockstate);
		});
	}

	/**
	 * Places a new portal in the {@link #getWorld()} and locates it.
	 */
	@Override
	public Optional<TeleportationRepositioner.Result> createPortal(BlockPos startPos, Direction.Axis enterAxis)
	{
		return this.placementBehavior.apply(this, startPos, enterAxis);
	}

	@Override
	@Nullable
	public PortalInfo getPortalInfo(Entity entity, ServerWorld destWorld, Function<ServerWorld, PortalInfo> defaultPortalInfo)
	{
		// Scale position
		WorldBorder worldborder = destWorld.getWorldBorder();
		double minX = Math.max(-2.9999872E7D, worldborder.getMinX() + 16.0D);
		double minZ = Math.max(-2.9999872E7D, worldborder.getMinZ() + 16.0D);
		double maxX = Math.min(2.9999872E7D, worldborder.getMaxX() - 16.0D);
		double maxZ = Math.min(2.9999872E7D, worldborder.getMaxZ() - 16.0D);
		double scaling = DimensionType.getTeleportationScale(entity.level.dimensionType(), destWorld.dimensionType());
		BlockPos scaledPos = new BlockPos(MathHelper.clamp(entity.getX() * scaling, minX, maxX), entity.getY(), MathHelper.clamp(entity.getZ() * scaling, minZ, maxZ));

		// Get info about current portal
		BlockPos portalPos = ObfuscationReflectionHelper.getPrivateValue(Entity.class, entity, "field_242271_ac");
		BlockState blockstate = entity.level.getBlockState(portalPos);
		Direction.Axis portalAxis;
		Vector3d offset;
		if (blockstate.hasProperty(BlockStateProperties.HORIZONTAL_AXIS))
		{
			portalAxis = blockstate.getValue(BlockStateProperties.HORIZONTAL_AXIS);
			TeleportationRepositioner.Result motionTpResult = TeleportationRepositioner.getLargestRectangleAround(portalPos, portalAxis, 21, Direction.Axis.Y, 21, bp -> entity.level.getBlockState(bp) == blockstate);
			offset = PortalSize.getRelativePosition(motionTpResult, portalAxis, entity.position(), entity.getDimensions(entity.getPose()));
		}
		else
		{
			portalAxis = Direction.Axis.X;
			offset = new Vector3d(0.5D, 0.0D, 0.0D);
		}

		// Find or create a new portal
		Optional<Result> result = this.findPortalAround(scaledPos, false);
		if (entity instanceof ServerPlayerEntity && !result.isPresent())
			result = createPortal(scaledPos, portalAxis);
		if (!result.isPresent())
			return null;

		// Get info from new portal
		PortalInfo portalInfo = PortalSize.createPortalInfo(destWorld, result.get(), portalAxis, offset, entity.getDimensions(entity.getPose()), entity.getDeltaMovement(), entity.yRot, entity.xRot);
		return new PortalInfo(new Vector3d(result.get().minCorner.getX() + 0.5, result.get().minCorner.getY() + 0.05, result.get().minCorner.getZ() + 0.5), portalInfo.speed, portalInfo.yRot, portalInfo.xRot);
	}

	/**
	 * Places this portal on highest block in the world, ignoring blocks specified
	 * in {@link #shouldIgnoreBlock(BlockState, BlockPos)}.
	 *
	 * @param teleporter
	 * @param startPos
	 * @param enterAxis
	 * @return {@link Optional}
	 */
	public static Optional<TeleportationRepositioner.Result> createAndFindPortalSurface(GelTeleporter teleporter, BlockPos startPos, Direction.Axis enterAxis)
	{
		ServerWorld world = teleporter.level;
		int x = startPos.getX();
		int y = world.getHeight();
		int z = startPos.getZ();

		// Calculate position
		BlockPos.Mutable mutablePos = new BlockPos.Mutable(x, y, z);
		int i = y;
		BlockState state = world.getBlockState(mutablePos);
		while (i > 0 && teleporter.shouldIgnoreBlock(state, mutablePos))
		{
			state = world.getBlockState(mutablePos.move(Direction.DOWN));
			i--;
		}

		if (i <= 0)
			y = teleporter.getDefaultHeight();
		else
			y = i + 1;

		// Place frame
		BlockState frameState = teleporter.getFrameBlock().get();
		for (int horizontalOffset = -1; horizontalOffset < 3; ++horizontalOffset)
		{
			for (int verticalOffset = -1; verticalOffset < 4; ++verticalOffset)
			{
				if (horizontalOffset == -1 || horizontalOffset == 2 || verticalOffset == -1 || verticalOffset == 3)
				{
					BlockPos pos2 = new BlockPos(x, y + verticalOffset, z + horizontalOffset);
					world.setBlock(pos2, frameState, 3);
				}
			}
		}

		// Place portal blocks
		BlockState portalState = teleporter.getPortalBlock().get().defaultBlockState().setValue(NetherPortalBlock.AXIS, Direction.Axis.Z);
		for (int horizontalOffset = 0; horizontalOffset < 2; ++horizontalOffset)
		{
			for (int verticalOffset = 0; verticalOffset < 3; ++verticalOffset)
			{
				BlockPos pos2 = new BlockPos(x, y + verticalOffset, z + horizontalOffset);
				world.setBlock(pos2, portalState, 18);
			}
		}

		// Add platform below portal
		boolean placePlatform = true;
		label0: for (int x1 = -1; x1 < 2; x1++)
		{
			for (int z1 = 0; z1 < 2; z1++)
			{
				BlockPos pos2 = new BlockPos(x + x1, y - 1, z + z1);
				BlockState existingState = world.getBlockState(pos2);
				if (!(teleporter.shouldIgnoreBlock(existingState, pos2) || existingState.getMaterial().isLiquid()) && existingState.getBlock() != frameState.getBlock())
				{
					placePlatform = false;
					break label0;
				}
			}
		}

		if (placePlatform)
			for (int x1 = -1; x1 < 2; x1++)
				for (int z1 = 0; z1 < 2; z1++)
					world.setBlockAndUpdate(new BlockPos(x + x1, y - 1, z + z1), frameState);

		// find and return portal
		return teleporter.findPortalAround(startPos, false);
	}

	/**
	 * Default code for the Teleporter with slight modification.
	 *
	 * @param teleporter
	 * @param startPos
	 * @param enterAxis
	 * @return {@link Optional}
	 */
	public static Optional<TeleportationRepositioner.Result> createAndFindPortalNether(GelTeleporter teleporter, BlockPos startPos, Direction.Axis enterAxis)
	{
		Direction direction = Direction.get(Direction.AxisDirection.POSITIVE, enterAxis);
		double d0 = -1.0D;
		BlockPos blockpos = null;
		double d1 = -1.0D;
		BlockPos blockpos1 = null;
		WorldBorder worldborder = teleporter.level.getWorldBorder();
		int i = teleporter.level.getHeight() - 1;
		BlockPos.Mutable blockpos$mutable = startPos.mutable();

		for (BlockPos.Mutable blockpos$mutable1 : BlockPos.spiralAround(startPos, 16, Direction.EAST, Direction.SOUTH))
		{
			int j = Math.min(i, teleporter.level.getHeight(Heightmap.Type.MOTION_BLOCKING, blockpos$mutable1.getX(), blockpos$mutable1.getZ()));
			if (worldborder.isWithinBounds(blockpos$mutable1) && worldborder.isWithinBounds(blockpos$mutable1.move(direction, 1)))
			{
				blockpos$mutable1.move(direction.getOpposite(), 1);

				for (int l = j; l >= 0; --l)
				{
					blockpos$mutable1.setY(l);
					if (teleporter.level.isEmptyBlock(blockpos$mutable1))
					{
						int i1;
						for (i1 = l; l > 0 && teleporter.level.isEmptyBlock(blockpos$mutable1.move(Direction.DOWN)); --l)
						{
						}

						if (l + 4 <= i)
						{
							int j1 = i1 - l;
							if (j1 <= 0 || j1 >= 3)
							{
								blockpos$mutable1.setY(l);
								if (teleporter.checkRegionForPlacement(blockpos$mutable1, blockpos$mutable, direction, 0))
								{
									double d2 = startPos.distSqr(blockpos$mutable1);
									if (teleporter.checkRegionForPlacement(blockpos$mutable1, blockpos$mutable, direction, -1) && teleporter.checkRegionForPlacement(blockpos$mutable1, blockpos$mutable, direction, 1) && (d0 == -1.0D || d0 > d2))
									{
										d0 = d2;
										blockpos = blockpos$mutable1.immutable();
									}

									if (d0 == -1.0D && (d1 == -1.0D || d1 > d2))
									{
										d1 = d2;
										blockpos1 = blockpos$mutable1.immutable();
									}
								}
							}
						}
					}
				}
			}
		}

		if (d0 == -1.0D && d1 != -1.0D)
		{
			blockpos = blockpos1;
			d0 = d1;
		}

		if (d0 == -1.0D)
		{
			blockpos = (new BlockPos(startPos.getX(), MathHelper.clamp(startPos.getY(), 70, teleporter.level.getHeight() - 10), startPos.getZ())).immutable();
			Direction direction1 = direction.getClockWise();
			if (!worldborder.isWithinBounds(blockpos))
			{
				return Optional.empty();
			}

			for (int l1 = -1; l1 < 2; ++l1)
			{
				for (int k2 = 0; k2 < 2; ++k2)
				{
					for (int i3 = -1; i3 < 3; ++i3)
					{
						BlockState blockstate1 = i3 < 0 ? teleporter.frameBlock.get() : Blocks.AIR.defaultBlockState();
						blockpos$mutable.setWithOffset(blockpos, k2 * direction.getStepX() + l1 * direction1.getStepX(), i3, k2 * direction.getStepZ() + l1 * direction1.getStepZ());
						teleporter.level.setBlockAndUpdate(blockpos$mutable, blockstate1);
					}
				}
			}
		}

		for (int k1 = -1; k1 < 3; ++k1)
		{
			for (int i2 = -1; i2 < 4; ++i2)
			{
				if (k1 == -1 || k1 == 2 || i2 == -1 || i2 == 3)
				{
					blockpos$mutable.setWithOffset(blockpos, k1 * direction.getStepX(), i2, k1 * direction.getStepZ());
					teleporter.level.setBlock(blockpos$mutable, teleporter.frameBlock.get(), 3);
				}
			}
		}

		BlockState blockstate = teleporter.portalBlock.get().defaultBlockState().setValue(NetherPortalBlock.AXIS, enterAxis);

		for (int j2 = 0; j2 < 2; ++j2)
		{
			for (int l2 = 0; l2 < 3; ++l2)
			{
				blockpos$mutable.setWithOffset(blockpos, j2 * direction.getStepX(), l2, j2 * direction.getStepZ());
				teleporter.level.setBlock(blockpos$mutable, blockstate, 18);
			}
		}

		return Optional.of(new TeleportationRepositioner.Result(blockpos.immutable(), 2, 3));
	}

	private boolean checkRegionForPlacement(BlockPos pos, BlockPos.Mutable mutablePos, Direction facing, int offset)
	{
		Direction direction = facing.getClockWise();

		for (int i = -1; i < 3; ++i)
		{
			for (int j = -1; j < 4; ++j)
			{
				mutablePos.setWithOffset(pos, facing.getStepX() * i + direction.getStepX() * offset, j, facing.getStepZ() * i + direction.getStepZ() * offset);
				if (j < 0 && !this.level.getBlockState(mutablePos).getMaterial().isSolid())
					return false;

				if (j >= 0 && !this.level.isEmptyBlock(mutablePos))
					return false;
			}
		}

		return true;
	}

	/**
	 * Determins how the portal generated should be placed.
	 *
	 * @author David
	 */
	public enum CreatePortalBehavior
	{
		NETHER(GelTeleporter::createAndFindPortalNether), ON_SURFACE(GelTeleporter::createAndFindPortalSurface);

		private final ICreatePortalFuncion function;

		CreatePortalBehavior(ICreatePortalFuncion function)
		{
			this.function = function;
		}

		public ICreatePortalFuncion get()
		{
			return this.function;
		}
	}

	/**
	 * Used to create a portal and get it's location.
	 *
	 * @author David
	 */
	@FunctionalInterface
	public interface ICreatePortalFuncion
	{
		Optional<TeleportationRepositioner.Result> apply(GelTeleporter teleporter, BlockPos startPos, Direction.Axis enterAxis);
	}
}
