package com.legacy.structure_gel.util;

/**
 * Used internally. You shouldn't need to override or call what this is
 * annotating. Anything marked with this is subject to be changed between
 * updates in ways that could be break if you reference it.
 *
 * @author David
 */
public @interface Internal
{

}
