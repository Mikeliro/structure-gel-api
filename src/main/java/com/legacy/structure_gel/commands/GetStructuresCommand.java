package com.legacy.structure_gel.commands;

import java.util.Collection;
import java.util.function.Supplier;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.StructureFeature;

public class GetStructuresCommand
{
	public static LiteralArgumentBuilder<CommandSource> get()
	{
		LiteralArgumentBuilder<CommandSource> command = Commands.literal("getstructures").executes(GetStructuresCommand::getStructures);
		return command;
	}

	public static int getStructures(CommandContext<CommandSource> context)
	{
		Biome biome = context.getSource().getLevel().getBiome(new BlockPos(context.getSource().getPosition()));
		context.getSource().sendSuccess(new StringTextComponent("[" + biome.getRegistryName().toString() + "]").withStyle(TextFormatting.GREEN), true);
		Collection<Supplier<StructureFeature<?, ?>>> structures = biome.getGenerationSettings().structures();
		if (structures.isEmpty())
			context.getSource().sendSuccess(new StringTextComponent(biome.getRegistryName().toString() + " has no structures."), true);
		else
			structures.forEach(supplier -> context.getSource().sendSuccess(new StringTextComponent(" - " + supplier.get().feature.getRegistryName().toString()), true));

		return structures.size();
	}
}
