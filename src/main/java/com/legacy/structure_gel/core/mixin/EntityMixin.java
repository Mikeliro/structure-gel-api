package com.legacy.structure_gel.core.mixin;

import com.legacy.structure_gel.blocks.GelPortalBlock;
import com.legacy.structure_gel.util.capability.GelCapability;
import com.legacy.structure_gel.util.capability.GelEntity;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Entity.class)
public class EntityMixin
{
	@Shadow
	public World level;
	@Shadow
	protected boolean isInsidePortal;
	@Shadow
	protected int portalTime;

	@Inject(at = @At("HEAD"), method = "handleNetherPortal()V", cancellable = true)
	private void updatePortal(CallbackInfo callback)
	{
		GelCapability.ifPresent((Entity) (Object) this, gelEntity ->
		{
			// in gel portal
			if (gelEntity.getPortal() != null)
			{
				// Server side
				if (this.level instanceof ServerWorld)
				{
					if (this.isInsidePortal)
					{
						GelPortalBlock portal = gelEntity.getPortal();
						int maxTime = portal.getMaxTimeInside(((Entity) (Object) this));
						ServerWorld destWorld = ((ServerWorld) this.level).getServer().getLevel(portal.getTeleporter((ServerWorld) this.level).getOpposite());

						if (destWorld != null && !this.isPassenger() && this.portalTime++ >= maxTime)
						{
							this.level.getProfiler().push("portal");
							this.portalTime = maxTime;
							this.setPortalCooldown();

							this.changeDimension(destWorld, portal.getTeleporter(destWorld));

							this.level.getProfiler().pop();
						}

						this.isInsidePortal = false;
						gelEntity.setPortal(null);
					}
					else
					{
						if (this.portalTime > 0)
							this.portalTime -= 4;
						if (this.portalTime < 0)
							this.portalTime = 0;
					}

					this.processPortalCooldown();
					callback.cancel();
				}

				gelEntity.setPortal(null);
			}
			// not in gel portal but in some portal
			else if (this.isInsidePortal)
				GelEntity.setPortalClient((Entity) (Object) this, null);
		});
	}

	@Shadow
	public boolean isPassenger()
	{
		throw new IllegalStateException("Mixin failed to shadow isPassenger()");
	}

	@Shadow
	public void setPortalCooldown()
	{
		throw new IllegalStateException("Mixin failed to shadow setPortalCooldown()");
	}

	@Shadow(remap = false)
	public Entity changeDimension(ServerWorld world, net.minecraftforge.common.util.ITeleporter teleporter)
	{
		throw new IllegalStateException("Mixin failed to shadow changeDimension()");
	}

	@Shadow
	protected void processPortalCooldown()
	{
		throw new IllegalStateException("Mixin failed to shadow decrementTimeUntilPortal()");
	}
}
