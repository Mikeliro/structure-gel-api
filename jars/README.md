## ❗❗❗ This method of using Structure Gel API as a dependency in your IDE has been DEPRECATED! Please refer to the `README.md` file in the root folder for new instructions on how to install the dependency. ❗❗❗

# Structure Gel API Jar Dependencies

These are deobfuscated jars of Structure Gel API releases, intended for developers to be able
to use in their IDE. [Do not redistribute.](https://moddinglegacy.com/ML-Lesser-License/)

If you're looking to download the mod to play, go to the Curse page and grab it from there.  
https://www.curseforge.com/minecraft/mc-mods/structure-gel-api

Naming conventions:
- `<MC version>-<Mod Version>`
- Mod Version: first.second.third.mapping
  - first: Major release of the mod. This number will only update when the code needs an overall rework.
  - second: Feature update. This number will only update when new features are introduced.
  - third: Bug fix update. This number will only update when a bug fix is released.
  - mapping: This letter will only be present if mappings have been updated and that's the only change.
    - May not be present.
    - Is not released to Curse since it doesn't matter for that.

Jar Version   | Forge Version     | Mapping Version
--------------|-------------------|------------------
1.16.4-1.7.0  | 1.16.4-35.1.0     | 20201028-1.16.3
1.16.4-1.7.1  | 1.16.4-35.1.0     | 20201028-1.16.3
1.16.4-1.7.2  | 1.16.4-35.1.0     | 20201028-1.16.3
1.16.4-1.7.3*  | 1.16.4-35.1.0     | 20201028-1.16.3

\* This version of Structure Gel API is the last version that will be distributed as a deobfuscated jar. This version and all future versions of Structure Gel API will now be under the Modding Legacy maven repository.