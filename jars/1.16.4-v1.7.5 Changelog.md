# 1.16.4-v1.7.5

## Additions
	- Added a crash log hook to make it clearer when a crash comes from this mod

## Changes
	- Removed the chat message that showed up when a mod removed the DataFixerUpper
		- Note that the bug is still present. It's up to mod developers to resave their structures for each Minecraft version to prevent the issue.