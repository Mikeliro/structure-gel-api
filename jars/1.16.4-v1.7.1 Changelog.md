# 1.16.4-v1.7.1
Hotfix to correct an issue with IStructurePieceTypes

## Fixes
	- Fixed structures having unknown piece types when updating